package com.vhiep.demo

import android.annotation.SuppressLint
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.vhiep.demo.databinding.FragmentDemoBinding


class DemoFragment : Fragment() {
    private lateinit var binding: FragmentDemoBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDemoBinding.bind(
            layoutInflater.inflate(
                R.layout.fragment_demo,
                container,
                false
            )
        )
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            reset.setOnClickListener {
                card.isVisible = true
            }
            container.setOnTouchListener { v, e ->
                if (e.action === MotionEvent.ACTION_DOWN) {
                    // Notify touch outside listener if user tapped outside a given view
                    if (card.isVisible) {
                        val viewRect = Rect()
                        card.getGlobalVisibleRect(viewRect)
                        if (!viewRect.contains(e.rawX.toInt(), e.rawY.toInt())) {
                            card.isVisible = false
                        }
                    }
                }
                // Further touch is not handled
                false
            }
        }
    }
}